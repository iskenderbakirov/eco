from django.urls import path
from main.views import IndexView, NewsView, contactsView, SingleView, PartnersView

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('news', NewsView.as_view(), name='news'),
    path('single', SingleView.as_view(), name='single'),
    path('partners', PartnersView.as_view(), name='partners'),
    path('contacts', contactsView.as_view(), name='contacts')
]