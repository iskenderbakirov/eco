from django.shortcuts import render
from django.views.generic import TemplateView
# Create your views here.

class IndexView(TemplateView):
    template_name = 'index.pug'

class NewsView(TemplateView):
    template_name = 'news.pug'

class SingleView(TemplateView):
    template_name = 'single.pug'

class PartnersView(TemplateView):
    template_name = 'partners.pug'

class contactsView(TemplateView):
    template_name = 'contacts.pug'